import { createRouter, createWebHistory } from 'vue-router'

// Define view routes
const routes = [
  {
    path: '/',
    name: 'Home',
    // As we don't part from any component, we can import each view as if it was a component
    component: () => import('../views/Inicio.vue')
  },
  {
    path: '/notes',
    name: 'Notes',
    component: () => import('../views/Notes.vue')
  },
  {
    path: '/list',
    name: 'List',
    component: () => import('../views/List.vue')
  },
  {
    path: '/list/:id',
    name: 'Pokemon',
    component: () => import('../views/Pokemon.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
